
class Gallery {

    constructor() {
        this._createDOM();
        this._registerEvents();
        this.currentFigure = null;
    }

    _createDOM() {
        const container = document.createElement("div");
        container.id = "gallery-container";
        const figure = document.createElement("div");
        figure.id = "gallery-figure";
        const image = document.createElement("img");
        image.id = "gallery-image";
        const caption = document.createElement("div");
        caption.classList.add("g-desc-fullscreen");

        figure.appendChild(image);
        figure.appendChild(caption);
        container.appendChild(figure);
        document.body.appendChild(container);

        this.container = container;
        this.figure = figure;
        this.image = image;
        this.caption = caption;
    }

    // registering mouse and keyboard events
    _registerEvents() {
        const thumbs = Array.from(document.getElementsByClassName('figure'));
        const validThumbs = thumbs.filter(th => this.isGalleryValid(th));
        const t = this;

        // register clicker on figures
        this.thumbs = validThumbs.map(function(el) {
            el.addEventListener('click', t.figureClickedWrapper(t));
            el.addEventListener('mouseover', t.figureMouseOver(t));
            el.addEventListener('mouseout', t.figureMouseOut(t));
            return el;
        });

        this.container.addEventListener('click', t.figureClickedWrapper(t));
        // register key strokes on document
        document.addEventListener('keyup', t.onKeyUpWrapper(t));
    }

    getPreviousFigure(figure) {
        const index = this.thumbs.indexOf(figure);
        if ((index === -1) || (index === 0)) {
            return null;
        }
        return this.thumbs[index-1];
    }

    getNextFigure(figure) {
        const index = this.thumbs.indexOf(figure);
        if ((index === -1) || (index === (this.thumbs.length - 1))) {
            return null;
        }
        return this.thumbs[index+1];
    }

    isGalleryValid(figure) {
        for (name of figure.classList.values()) {
            if (name.startsWith("g-col")) {
                return true;
            }
        }
        return false;
    }

    showFigure(figure) {
        this.currentFigure = figure;

        if (figure === null) {
          this.dismiss();
          return
        }

        const captionElement = figure.getElementsByClassName("g-desc")[0];
        if (captionElement) {
            this.caption.style.opacity = 1.0;
            this.caption.innerText = captionElement.innerText;
        } else {
            this.caption.style.opacity = 0.0;
        }

        this.container.style.display = 'block';
        this.container.style.top = window.scrollY + "px";
        this.image.src = figure.children[0].src;
        this.image.srcset = figure.children[0].srcset;
        //we don't want to copy sizes, bcs of 800px layout in post.. so in fullscreen browser will use 100vw as default
        this.disableScroll();

        //prefetch next image
        const nextFigureCache = this.getNextFigure(figure);
        if (nextFigureCache !== null) {
            const cacheImage = new Image();
            cacheImage.src = nextFigureCache.children[0].src;
            cacheImage.srcset = nextFigureCache.children[0].srcset;
        }
    }

    dismiss() {
        this.currentFigure = null;
        this.container.style.display = 'none';
        this.image.src = '';
        this.image.srcset = '';
        this.enableScroll();
    }

    figureClickedWrapper(context) {
        return function(event) {
            const path = event.path || (event.composedPath && event.composedPath());
            if (event.srcElement.tagName.toLowerCase() === "img") {
                context.currentFigure === null
                    ? context.showFigure(path[1])
                    : context.dismiss();
            }
        }
    }

    figureMouseOver(context) {
        return function(event) {
            const titleElement = event.srcElement.parentElement.getElementsByClassName("g-desc")[0];
            if (titleElement) {
                titleElement.style.opacity = 1.0;
            }
        }
    }

    figureMouseOut(context) {
        return function(event) {
            const titleElement = event.srcElement.parentElement.getElementsByClassName("g-desc")[0];
            if (titleElement) {
                titleElement.style.opacity = 0.0;
            }
        }
    }

    onKeyUpWrapper(context) {
        return function(key) {
            if (key.key === "ArrowLeft" && context.currentFigure !== null) {
                const prevFigure = context.getPreviousFigure(context.currentFigure);
                context.showFigure(prevFigure);
            } else if (key.key === "ArrowRight" && context.currentFigure !== null) {
                const nextFigure = context.getNextFigure(context.currentFigure);
                context.showFigure(nextFigure);
            }
        }
    }

    theMouseWheel(e) {
        preventDefault(e);
    }

    theKeyDown(e) {
        const keyCodes = [32, 33, 34, 38, 40];
        if (keyCodes.includes(e.which)) {
            preventDefault(e);
        }
    }

    disableScroll() {
        if (window.addEventListener) {
            window.addEventListener('DOMMouseScroll', this.theMouseWheel, false);
            window.addEventListener('keydown', this.theKeyDown, false);
        }
        window.onmousewheel = document.onmousewheel = this.theMouseWheel;
        window.onkeydown = document.onkeydown = this.theKeyDown;
    }

    enableScroll() {
        if (window.removeEventListener) {
            window.removeEventListener('DOMMouseScroll', this.theMouseWheel, false);
            window.removeEventListener('keydown', this.theKeyDown, false);
        }
        window.onmousewheel = document.onmousewheel = null;
        window.onkeydown = document.onkeydown = null;
    }
}

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function galleryClicker() {
    const galleryClicker = new Gallery();
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', galleryClicker);
} else {
    galleryClicker();
}

InstantClick.on('change', function() {
    galleryClicker();
});
